FROM python:2.7.13-slim

COPY app/ /app/

WORKDIR /app

RUN pip install -r requirements.txt 

EXPOSE 8000

ENTRYPOINT ["/usr/local/bin/gunicorn","-w","4","-b",":8000","helloworld:app"]
